# coding=utf-8

from sqlalchemy import Column, String, Integer, Date

from basepoissons import Base


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    prenom = Column(String)
    nom = Column(String)
    telephone = Column(String)
    mail = Column(String)
    pass_word = Column(String)
    adresse = Column(String)
    region = Column(String)


    def __init__(self, prenom, nom, telephone, mail, pass_word, adresse, region):
        self.prenom = prenom
        self.nom = nom
        self.telephone = telephone
        self.mail
        self.pass_word = pass_word
        self.adresse = adresse
        self.region = region
