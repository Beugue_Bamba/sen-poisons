# coding=utf-8

from sqlalchemy import Column, String, Integer, Numeric, Date, ForeignKey
from sqlalchemy.orm import relationship

from basepoissons import Base

class Commande(Base):
    __tablename__ = 'commandes'

    id = Column(Integer, primary_key=True)
    nom = Column(String)
    photo = Column(String)
    prix_unitaire = Column(Numeric)
    quantite = Column(Numeric)
    prix_total = Column(Numeric)
    appeller = Column(String)
    livrer = Column(String)
    prenom_client = Column(String)
    nom_client = Column(String)
    num_client = Column(String)
    mail_client = Column(String)
    adresse_client = Column(String)
    region = Column(String)
    date_commande = Column(String)
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship("User", backref="modeles")

    


    def __init__(self, nom, photo, prix_unitaire, quantite, prix_total, appeller, livrer, prenom_client, nom_client, num_client,mail_client, adresse_client, region, date_commande, user):
        self.nom = nom
        self.photo = photo
        self.prix_unitaire = prix_unitaire
        self.quantite = quantite
        self.prix_total = prix_total
        self.appeller = appeller
        self.livrer = livrer
        self.prenom_client = prenom_client
        self.nom_client = nom_client
        self.num_client = num_client
        self.mail_client = mail_client
        self.adresse_client = adresse_client
        self.region = region
        self.date_commande = date_commande
        self.user = user
