# import flask_login
from flask import Flask, render_template, redirect, url_for, request, flash, session, g,jsonify

app = Flask(__name__)

########################################################################################"

# import flask_login
from flask import Flask, render_template, redirect, url_for, request, flash, session, g,jsonify
import psycopg2
import datetime
import re  # verification des mails, noms, etc
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash, check_password_hash
import datetime
from PIL import Image
# from resizeimage import resizeimage

import os
from werkzeug.utils import secure_filename
import pandas as pd
import numpy as np
from flask_paginate import Pagination, get_page_args
from flask_sqlalchemy import SQLAlchemy

from flask_cors import CORS
from sqlalchemy import or_, cast, String
# from flask_caching import Cache




# 1 - imports tables of data base
from datetime import date

from basepoissons import Sessionalchemy, engine, Base
from produits import Commande
from users import User
from djeun import Djeun

# 2 - generer le shema la base de données
Base.metadata.create_all(engine)
######################################FLASK MIGRATE###################################
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
#######################################################################################

SQLALCHEMY_TRACK_MODIFICATIONS = False

###################### CREATION DE MOT DE PASS ALEATOIRE ##################
import string
from random import sample

app = Flask(__name__)
CORS(app)

# define the cache config keys, remember that it can be done in a settings file
app.config['CACHE_TYPE'] = 'simple'
# register the cache instance and binds it on to your app 
# app.cache = Cache(app) 

HOSTNAME = '127.0.0.1'
PORT = '5432'
DATABASE = 'dbpoissons'
USERNAME = 'postgres'
PASSWORD = 'Kh@dim1991'


app.config["IMAGE_UPLOADS"] ="/home/mamadou/Téléchargements/poison/static/images"
app.config["ALLOWED_IMAGE_EXTENSIONS"] = ["JPEG", "JPG", "PNG", "GIF", "tiff", "HEIF", "HEIC"]
ALLOWED_EXTENSIONS = set([ 'png', 'jpg', 'jpeg', 'gif', 'tiff', 'heif', 'heic'])

app.config["MAX_IMAGE_FILESIZE"] = 0.5 * 4050 * 4050
app.config['SQLALCHEMY_DATABASE_URI']="postgres+psycopg2://{}:{}@{}:{}/{}".format (USERNAME,PASSWORD,HOSTNAME,PORT,DATABASE)
Sessionalchemy = Sessionalchemy()

db = SQLAlchemy(app)

migrate = Migrate(app,db)
manager = Manager(app)
manager.add_command('db',MigrateCommand)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def allowed_image(filename):
    if not "." in filename:
        return False

    ext = filename.rsplit(".", 1)[1]
    if ext.upper() in app.config["ALLOWED_IMAGE_EXTENSIONS"]:
        return True
    else:
        return False

def allowed_image_filesize(filesize):

    if int(filesize) <= app.config["MAX_IMAGE_FILESIZE"]:
        return True
    else:
        return False


## Add users

## Admin Khadim
id_kha = 1
prenom_kha = "Khadim"
nom_kha = "NGING"
telephone_kha = "771846968"
mail_kha = "khadim@gmail.com"
adresse_kha = "Camberen"
region = ""
pass_word_kha = "khadim@poisson2020"
pass_wordx_kha = generate_password_hash(pass_word_kha)


## Admin SARR
id_sar = 2
prenom_sar = "Mamadou"
nom_sar = "SARR"
telephone_sar = "777886847"
mail_sar = "mamadoutive@yahoo.fr"
adresse_sar = "Guédiawaye"
pass_word = "mamadou@poisson2020"
pass_wordx_sar = generate_password_hash(pass_word)

## Admin Ndiaga
id_ndia = 3
prenom_ndia = "Ndiaga"
nom_ndia = "GAYE"
telephone_ndia = "771817731"
mail_ndia = "ndiaga@gmail.com"
adresse_ndia = "Thies"
pass_word_ndia = "ndiaga@poisson2020"
pass_wordx_ndia = generate_password_hash(pass_word_ndia)


## Client Baba
id_ba = 4
prenom_ba = "Baba"
nom_ba = "MAL"
telephone_ba = "777886848"
mail_ba = "baba@gmail.com"
adresse_ba = "Dakar"
pass_word_ba = "babamal2020"
pass_wordx_ba = generate_password_hash(pass_word_ba)


exits_kha = Sessionalchemy.query(User.id).filter(User.telephone==telephone_kha).scalar()
exits_sar = Sessionalchemy.query(User.id).filter(User.telephone==telephone_sar).scalar()
exits_ndia = Sessionalchemy.query(User.id).filter(User.telephone==telephone_ndia).scalar()

if exits_kha == None and exits_sar == None and exits_ndia == None:
    use_kha = User(prenom_kha, nom_kha, telephone_kha, mail_kha, pass_wordx_kha, adresse_kha, region)
    use_sar = User(prenom_sar, nom_sar,  telephone_sar, mail_sar, pass_wordx_sar, adresse_sar, region)
    use_ndia = User(prenom_ndia, nom_ndia,  telephone_ndia, mail_ndia, pass_wordx_ndia, adresse_ndia, region)
    use_ba = User(prenom_ba, nom_ba, telephone_ba, mail_ba, pass_wordx_ba, adresse_ba, region)


    Sessionalchemy.add(use_kha)
    Sessionalchemy.add(use_sar)
    Sessionalchemy.add(use_ndia)
    Sessionalchemy.add(use_ba)
    Sessionalchemy.commit()



################################### LOGIN #################################

#### code pour generer un secret key: python -c 'import os; print(os.urandom(16))'

app.secret_key = "b\x062AA\xe5A\xa7?\t*%\x1a'9\x9f\xd2"

idd = []


@app.route('/login/', methods=['GET', 'POST'])
def login():
    count=0
    # Output message if something goes wrong...
    msg = ''
    # Check if "username" and "password" POST requests exist (user submitted form)
    if request.method == 'POST' and 'telephon' in request.form and 'password' in request.form :
      # Create variables for easy access
        telephon = request.form['telephon']
        pass_wordd = request.form['password']

        hash = []
        exits = Sessionalchemy.query(User.id).filter(User.telephone==telephon).scalar()
        if exits != None:
            data = Sessionalchemy.query(User)\
                .filter(User.telephone == telephon)\
                .all()
            
            for i in data:
                pass_word = i.pass_word
                hash.append(pass_word)
            # pass_words = session.get(pass_word, None)
            val1 = check_password_hash(hash[0], pass_wordd)
            if val1 == True:
                person = Sessionalchemy.query(User)\
                    .filter(User.telephone == telephon)\
                    .filter(User.pass_word == hash[0])\
                    .all()
                
                datas = session.get('datas')              
                if (telephon != 771846968  and pass_wordd != 'khadim@poisson2020') and (telephon != 777886847  and pass_wordd != 'mamadou@poisson2020') and (telephon != 771817731  and pass_wordd != 'ndiaga@poisson2020'):
                    for i in person:
                        id = i.id
                        prenom = i.prenom
                        nom = i.nom
                        telephone = i.telephone
                        adresse = i.adresse
                        regions = i.region
                        mail = i.mail
                        
                        # for i in account:
                        # Create session data, we can access this data in other routes
                        session['loggedin'] = True
                        session['id'] = id
                        session['telephon'] = telephone
                        user = Sessionalchemy.query(User).filter_by(id=id).first()
                        
                        for i in datas:
                            produit = i['val2']
                            prix = i['val3']
                            qty = i['val4']
                            total = i['val5']
                            # print(produit, "**", prix, "***", qty, "***", total)
                            photoo = Sessionalchemy.query(Djeun.photo)\
                                    .filter(Djeun.nom==produit)\
                                    .first()

                            appeller = "NON"
                            livrer = "NON"
                            date_commande = str(datetime.datetime.now())
                            user = Sessionalchemy.query(User).filter_by(id=id).first()
                            
                            comm = Commande(produit, photoo, prix, qty, total, appeller, livrer,prenom,nom, telephone, mail, adresse, regions, date_commande, user)
                            Sessionalchemy.add(comm)
                            Sessionalchemy.commit()
                    flash("Felicitation, vous avez fait votre commande avec succé, notre équipe vous contactera pour la livraison!", "succes")
                    return redirect(url_for('client'))
                else:
                    for i in person:
                        id = i.id
                        nom = i.nom
                        telephone = i.telephone
                    
                        session['loggedin'] = True
                        session['id'] = id
                        session['telephon'] = telephone
                        return redirect(url_for('chef'))
                    
            else:
                flash("Le mot de pass tapé est incorrect!", "danger")
        else:
            flash("Le numéro téléphone est incorrect!", "danger")                        
    return render_template('login_register.html')

################################### Inscription Clients #################################


@app.route('/Inscription_clients', methods=['GET', 'POST'])
def Inscription_clients():
    msggz=''
    
    if request.method == 'POST' and 'nom' in request.form and 'telephone' in request.form and 'telephone2' in request.form and 'adresse' in request.form and 'password' in request.form:
        prenom = request.form.get("prenom")
        nom = request.form.get("nom")
        telephone = request.form.get("telephone")
        telephone2 = request.form.get("telephone2")
        region = request.form.get("region")
        adresse = request.form.get("adresse")
        mail = request.form.get("mail")
        pass_wordd = request.form.get('password')
        pass_wordd22 = request.form.get('password22')

        if pass_wordd == pass_wordd22:
            if len(pass_wordd) >=6:
                pass_word = generate_password_hash(pass_wordd) #hachage password
                date_inscription = str(datetime.datetime.now())
                if telephone == telephone2:
                    exits = Sessionalchemy.query(User.id).filter(User.telephone==telephone).scalar()
                    if exits == None:
                        users = User(prenom, nom, telephone, mail, pass_word, adresse, region)
                        Sessionalchemy.add(users)
                        Sessionalchemy.commit()
                        flash("Felicitation, vous avez créé votre compte avec succés!", "succes")

                        return redirect(url_for('client'))
                        
                    else:
                        flash("Le numéro de telephone existe déjà!","danger")
                else:                    
                    flash("Vérifiez le numéro de téléphone saisi svp!","danger") 
            else:
                flash("Le mot de passe saisi est trés faible, augmentez le nombre au minimum 6 charactéres.","danger")
        else:
            flash("Les mots de passes ne sont pas identiques!","danger") 

    # Show registration form with message (if any) 
    return render_template('creatCompte.html')


########################################  COMPTE CLIENTS ####################################"

@app.route('/client', methods=['GET', 'POST'])
# @app.cache.cached(timeout=3600)     # mettre en cache par 1h

def client():
    if session.get('id'):
        moi = []
        mycommande=[]
        commnonlivrer = []
        commlivrer = []
        dd=""
        id_user = session.get('id',None)
        
        nom_user = Sessionalchemy.query(User.nom)\
            .filter_by(id=id_user).first()


        ############################## commandes non livrées ###########################
        
        commnonlivre = Sessionalchemy.query(Commande)\
            .filter(Commande.livrer=='NON')\
            .filter(Commande.user_id==id_user)\
            .all()

        for i in commnonlivre:
            idq = i.id
            nom = i.nom
            photo = i.photo
            prixunit = i.prix_unitaire
            prixtot = i.prix_total
            qte = i.quantite
            telephone = i.num_client
            mail = i.mail_client
            adresse = i.adresse_client
            date = i.date_commande
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            mycommande.append((idq, nom, photo, prixunit, prixtot, qte, telephone, mail, adresse, date, prenomcl, nomcl))
        if len(mycommande) != 0 :
            commnonlivrer = mycommande
        else:
            return redirect(url_for('commandenonlivrernull'))
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
    else:
        return redirect(url_for('login'))
    
    return render_template('client.html', commnonlivrer=commnonlivrer, moi=moi) #client.html


@app.route('/commandelivrer', methods=['GET', 'POST'])
def commandelivrer():
    if session.get('id'):
        moi = []
        mycommande = []
        commlivrer = []
        id_user = session.get('id',None)
        commandelivre = Sessionalchemy.query(Commande)\
            .filter(Commande.livrer=='OUI')\
            .filter(Commande.user_id==id_user)\
            .order_by(Commande.date_commande)\
            .all()

        for i in commandelivre:
            id = i.id
            nom = i.nom
            photo = i.photo
            prixunit = i.prix_unitaire
            prixtot = i.prix_total
            qte = i.quantite
            nomclient = i.nom_client
            telephone = i.num_client
            mail = i.mail_client
            adresse = i.adresse_client
            date = i.date_commande
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            mycommande.append((id, nom, photo, prixunit, prixtot, qte, nomclient, telephone, mail, adresse, date, prenomcl, nomcl))
        if len(mycommande) != 0 :
            commlivrer = mycommande
        else:
            return redirect(url_for('commandelivrernull'))
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
    else:
        return redirect(url_for('login'))
    return render_template('commandelivrer.html', commlivrer=commlivrer, moi=moi)

@app.route('/commandenonlivrer', methods=['GET', 'POST'])
def commandenonlivrer():
    moi = []
    mycommande = []
    commnonlivrer = []
    if session.get('id'):
        id_user = session.get('id',None)
        commnonlivre = Sessionalchemy.query(Commande)\
            .filter(Commande.livrer=='NON')\
            .filter(Commande.user_id==id_user)\
            .all()

        for i in commnonlivre:
            id = i.id
            nom = i.nom
            photo = i.photo
            prixunit = i.prix_unitaire
            prixtot = i.prix_total
            qte = i.quantite
            telephone = i.num_client
            mail = i.mail_client
            adresse = i.adresse_client
            date = i.date_commande
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            mycommande.append((id, nom, photo, prixunit, prixtot, qte, telephone, mail, adresse, date, prenomcl, nomcl))
            moi.append((prenomcl,nomcl))
        if len(mycommande) != 0 :
            commnonlivrer = mycommande
        else:
            return redirect(url_for('commandenonlivrernull'))
    else:
        return redirect(url_for('login'))

    return render_template('commandenonlivrer.html', commnonlivrer=commnonlivrer, moi=moi)



@app.route('/commandenonlivrernull', methods=['GET', 'POST'])
def commandenonlivrernull():
    if session.get('id'):
        moi = []
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenomcl = i.prenom
            nomcl = i.nom
            moi.append((prenomcl,nomcl))
    else:
        return redirect(url_for('login'))
    return render_template('commandenonlivrernull.html', moi=moi)

@app.route('/commandelivrernull', methods=['GET', 'POST'])
def commandelivrernull():
    if session.get('id'):
        moi = []
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenomcl = i.prenom
            nomcl = i.nom
            moi.append((prenomcl,nomcl))
    else:
        return redirect(url_for('login'))

    return render_template('commandelivrernull.html', moi=moi)


####################################### PARTIE CHEF ###############################

@app.route('/chef', methods=['GET', 'POST'])
def chef():
    moi = []
    liste = []
    new_commandes = []
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        new_commande = []
        comm = Sessionalchemy.query(Commande)\
            .distinct()\
            .count()
        commnonapp = Sessionalchemy.query(Commande)\
            .filter(Commande.appeller=='NON')\
            .distinct()\
            .count()
        commnonliv = Sessionalchemy.query(Commande)\
            .filter(Commande.livrer=='NON')\
            .distinct()\
            .count()

        commande = Sessionalchemy.query(Commande)\
            .filter(Commande.livrer=='NON')\
            .all()
        
        commliv = Sessionalchemy.query(Commande)\
            .filter(Commande.livrer=='OUI')\
            .distinct()\
            .count()

        for i in commande:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix_unitaire = i.prix_unitaire
            quantite = i.quantite
            prix_total = i.prix_total
            appeller = i.appeller
            livrer = i.livrer
            telephone = i.num_client
            adresse = i.adresse_client
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            region = i.region
            new_commande.append((id, photo, nom, prix_unitaire, quantite, telephone, adresse, prix_total, appeller, livrer, prenomcl, nomcl, region))

        
        if len(new_commande) != 0:
            new_commandes = new_commande
        else:
            return redirect(url_for('admin'))

        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
    else:
        return redirect(url_for('login')) 

    return render_template('user.html', new_commandes=new_commandes, comm=comm, commnonapp=commnonapp, commnonliv=commnonliv, commliv=commliv, moi=moi)

# page administrateurs avec 0 modéle invalide
@app.route('/admin', methods=['GET', 'POST'])
def admin():   
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        moi=[]
        comm = Sessionalchemy.query(Commande)\
            .distinct()\
            .count()
        commnonapp = Sessionalchemy.query(Commande)\
            .filter(Commande.appeller=='NON')\
            .distinct()\
            .count()
        commnonliv = Sessionalchemy.query(Commande)\
            .filter(Commande.livrer=='NON')\
            .distinct()\
            .count()
        commliv = Sessionalchemy.query(Commande)\
            .filter(Commande.livrer=='OUI')\
            .distinct()\
            .count()
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
        
    else:
        return redirect(url_for('login')) 
    return render_template('admin.html', comm=comm, commnonapp=commnonapp, commnonliv=commnonliv, moi=moi, commliv=commliv)

@app.route('/liste', methods=['GET', 'POST'])
def liste():   
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        moi = []
        prod = []
        prods = []
        djeun = Sessionalchemy.query(Djeun)\
            .all()

        for i in djeun:
            idd = i.id
            photo = i.photo
            nom = i.nom
            prix = i.prix

            prod.append((idd, photo,nom,prix))
        if len(prod) != 0:
            prods = prod
        else:
            return redirect(url_for('listevide'))
        
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))    
    else:
        return redirect(url_for('login')) 
    return render_template('liste.html', prods=prods, moi=moi)

@app.route('/listevide', methods=['GET', 'POST'])
def listevide(): 
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        moi = []  
        comm = Sessionalchemy.query(Commande)\
            .distinct()\
            .count()
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
    else:
        return redirect(url_for('login')) 
    return render_template('listevide.html', moi=moi)

@app.route('/ajoutprod', methods=['GET', 'POST'])
def ajoutprod():   
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        nom = request.form.get("nameq")
        prix = request.form.get("prixq")
        old_price = request.form.get("oldprice")       
        images = request.files.getlist("imageq")
        if len(images)>1:
            flash("Vous avez selectionnez plus d'une image, revoyez svp la saisie.","danger")
            
        else:
            for image in images:
                if image.filename == "":
                    flash("Entrez svp une image", "avertissement")
                    return redirect(request.url)

                if allowed_image(image.filename):
                    filename = secure_filename(image.filename)
                    filenam, file_extension = os.path.splitext(filename)
                    hash = generate_password_hash(filenam)
                    hash=hash[-10:]
                    d = hash+file_extension
                    image.save(os.path.join(app.config["IMAGE_UPLOADS"], d))
                    djeunn = Djeun(nom, d, prix, old_price)
                    Sessionalchemy.add(djeunn)
                    Sessionalchemy.commit()
                
                    flash("Un nouveau produit est ajouté sur la liste.","succes")   
                else:
                    return "onk"
    else:
        return redirect(url_for('login')) 
    return render_template('ajoutprod.html')

@app.route('/appel/<string:idap>', methods=['GET'])
def appel(idap):
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:  
        val = Sessionalchemy.query(Commande)\
                .filter(Commande.id==idap).first()
        val.appeller = 'OUI'
        Sessionalchemy.commit()
        flash("Vous avez déja appelé le client, veuillez bien noter son adresse.","succes")
        return redirect(url_for('chef'))
    else:
        return redirect(url_for('login'))

@app.route('/livr/<string:idap>', methods=['GET'])
def livr(idap):
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        val = Sessionalchemy.query(Commande)\
                .filter(Commande.id==idap).first()
        val.livrer = 'OUI'
        Sessionalchemy.commit()
        flash("Vous avez déja livré le client, veuillez bien noter ses informations.","succes")
        return redirect(url_for('chef'))

    else:
        return redirect(url_for('login'))


@app.route('/valider/<string:id_ap>', methods=['GET'])
def valider(id_ap):
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3: 
        session['modifid'] = id_ap
        modif = []
        admin = Sessionalchemy.query(Djeun)\
            .filter(Djeun.id==id_ap)
        for i in admin:
            photo = i.photo
            nom = i.nom
            prix = i.prix
            modif.append((photo,nom,prix))
        
        # admin.user_id = ids
        Sessionalchemy.commit()
    
    else:
        return redirect(url_for('login'))

    return render_template('modif.html', modif=modif)

@app.route('/modif', methods=['GET', 'POST'])
def modif():
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        if request.method == 'POST':
            ids = session.get('modifid')
            nom = request.form.get("nameq")
            prix = request.form.get("prixq")
            val = Sessionalchemy.query(Djeun)\
                .filter(Djeun.id==ids).first()
            val.nom = nom
            val.prix = prix
            Sessionalchemy.commit()
            flash("Le produit est modifié avec succé.","succes")
        return redirect(url_for('chef'))
    else:
        return redirect(url_for('login'))

@app.route('/supprimer/<string:id_ap>', methods=['GET'])
def supprimer(id_ap):
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        phot=[]
        val =Sessionalchemy.query(Djeun)\
            .filter(Djeun.id==id_ap)\
                .all()
        for i in val:
            photo = i.photo
            phot.append(photo)
        print(phot)
        Sessionalchemy.query(Djeun)\
            .filter(Djeun.id==id_ap).delete()
        Sessionalchemy.commit()
        
        os.remove(os.path.join(app.config["IMAGE_UPLOADS"], phot[0]))

        flash("Le produit est supprimé de la liste.","danger")

        return redirect(url_for('chef'))

    else:
        return redirect(url_for('login'))

# Page Accueil

@app.route('/')
# # @app.cache.cached(timeout=3600)     # mettre en cache chaque 1h
def accueil():
    ret = "yes accueil"
    page = int(request.args.get('page', 1))
    per_page = 10
    offset = (page - 1) * per_page

    djeuns = Sessionalchemy.query(Djeun)\
        .all()
    djeun = []
    for i in djeuns:   
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        old = i.old_prix
        djeun.append((photo, nom, prix, id, old))
    total = len(djeun)
    paginationUsers = djeun[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('index.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


# #####*********************************************barre de recherche****************************************************#########
# @app.route('/search/<val>')
@app.route('/search', methods=['GET', 'POST'])
def search():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    searchh=[]
    if request.method == "POST":
        vall=request.form.get('vals')   
        val=vall.lower()
        search = "%{}%".format(val) 
        valeurs = Sessionalchemy.query(Djeun)\
            .filter(or_(Djeun.nom.ilike(search), cast( Djeun.prix, String ).ilike(search)))\
            .all()

        datas = []
        for i in valeurs:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix = i.prix
            datas.append((photo, nom, prix, id))

        if len(datas)!=0: 
            total = len(datas)
            paginationUsers=datas[offset: offset + per_page]
            pagination = Pagination(page=page, per_page=per_page, total=total,
                                    css_framework='bootstrap4')
        else:
            return  redirect(url_for('vide')) 
    else:
        return  redirect(url_for('accueil')) 
    return render_template('catalog_fullwidth.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)
@app.route('/vide',methods=['GET', 'POST'])
def vide():
    return render_template('vide.html')

####################### Partie détail ##############################
@app.route('/detail&<string:id_ap>', methods=['GET'])
def detail(id_ap):
    page = int(request.args.get('page', 1))
    per_page = 10
    offset = (page - 1) * per_page

    dieun=[]
    poissons = []
    modes = Sessionalchemy.query(Djeun)\
        .filter(Djeun.id==id_ap)\
        .all()
    tell = []
    for i in modes: 
        id = i.id  
        photo = i.photo
        nom = i.nom
        prix = i.prix
        tell.append((id, photo, nom, prix))
    if len(tell) != 0:
        dieun = tell
    else:
        return  redirect(url_for('accueil'))
    
    values = Sessionalchemy.query(Djeun)\
        .all()
    val = []
    for i in values: 
        id = i.id  
        photo = i.photo
        nom = i.nom
        old = i.old_prix
        prix = i.prix
        val.append((id, photo, nom, prix, old))
    
    if len(val) != 0:
        values = val
    else:
        return  redirect(url_for('accueil'))
    

    total = len(values)
    paginationUsers = values[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    # return render_template('index.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)



    return render_template('detailss.html',dieun=dieun, users=paginationUsers, page=page, per_page=per_page,pagination=pagination)
    # return render_template('panier.html',dieun=dieun)

@app.route('/panier')
def panier():

    return render_template('panier.html')


@app.route('/checkout')
def checkout():

     return render_template('checkout.html')

@app.route('/about')
def about():

     return render_template('about.html')

@app.route('/catalog')
def catalog():
     ret = "yes accueil"
     page = int(request.args.get('page', 1))
     per_page = 10
     offset = (page - 1) * per_page

     vendeur=[]
     id_user = session.get('id',None)

     djeuns = Sessionalchemy.query(Djeun)\
          .all()
     djeun = []
     for i in djeuns:   
          photo = i.photo
          nom = i.nom
          prix = i.prix
          id = i.id
          djeun.append((photo, nom, prix, id))
                                        
     total = len(djeun)
     paginationUsers = djeun[offset: offset + per_page]
     pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')

     return render_template('catalog_fullwidth.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

@app.route('/acheter&<string:idap>', methods = ['GET','POST'])
def acheter(idap):
     achat=[]
     session['idachet'] = idap 
     val =Sessionalchemy.query(Djeun)\
          .filter(Djeun.id==idap)\
          .all()
     for i in val:
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        achat.append((photo,nom,prix,id))
        print(achat)
     if len(achat)!=0:
        achat = achat
     else:
        return  redirect(url_for('catalog'))

     return render_template('shopping_cart.html', achat=achat)

@app.route('/contact')
def contact():

     return render_template('contact.html')

@app.route('/command', methods=['GET', 'POST'])
def command():
     comm=[]
     if request.method == 'POST':
          idacheter = session.get('idachet')
          prixs = request.form['prix']
          qte = request.form['qte']
          prixtot = int(prixs)*int(qte)
          appeller = "NON"
          livrer = "NON"

          val =Sessionalchemy.query(Djeun)\
               .filter(Djeun.id==idacheter)\
               .all()
          for i in val:
               photo = i.photo
               nom = i.nom
               prix = i.prix
               comm.append((photo,nom,prix))
          a = comm[0]
        #   commandee = Commande(a[1], a[0], prixs, qte, prixtot, appeller, livrer, None, None, None)
        #   Sessionalchemy.add(commandee)
        #   Sessionalchemy.commit()

     return redirect(url_for('accueil'))

@app.route('/ajoutcomm&<string:idcom>', methods=['GET', 'POST'])
def ajoutcomm(idcom):
     print(idcom)
     
     

     return redirect(url_for('accueil'))


@app.route('/getpanier', methods=['GET','POST'])
def getpanier():
    if request.method == "POST":   
        datas = request.json
        # print(datas)
        if len(datas) != 0 :
            session['datas'] = datas
            if session.get('id'):
                print("yes nice ok")
                return redirect(url_for('frompanier'))
            else:
                return redirect(url_for('login'))
        else:
            flash("Attention: vous avez pas bien inseré le produit dans le panier, merci de choisir un produit, de definir sa quantité et de valider à nouveau !", "danger")
            return  redirect(url_for('catalog'))
    return redirect(url_for('client'))

@app.route('/frompanier')
def frompanier():
    datas = session.get('datas')
    ids = session.get('id')
    print(ids, datas)
    appeller = "NON"
    livrer = "NON"
    date_commande = str(datetime.datetime.now())

    cli = Sessionalchemy.query(User)\
        .filter(User.id==ids)\
        .all()
    user = Sessionalchemy.query(User).filter_by(id=ids).first()
    for j in cli:
        prencli = j.prenom
        nomcli = j.nom
        phoncli = j.telephone
        mailcli = j.mail
        adresssecli = j.adresse
        regioncli = j.region
        for i in datas:
            produit = i['val2']
            prix = i['val3']
            qty = i['val4']
            total = i['val5']
            print(produit, "**", prix, "***", qty, "***", total)
            print(prencli, "**", nomcli, "***", phoncli, "***", mailcli)
            photoo = Sessionalchemy.query(Djeun.photo)\
                        .filter(Djeun.nom==produit)\
                        .first()  
          
            comm = Commande(produit, photoo, prix, qty, total, appeller, livrer,prencli,nomcli, phoncli, mailcli, adresssecli, regioncli, date_commande, user)
            Sessionalchemy.add(comm)
            Sessionalchemy.commit()
    flash("Felicitation, vous avez fait votre commande avec succé, notre équipe vous contactera pour la livraison!", "succes")
    return redirect(url_for('client'))
    



#########################################  DECONNEXION  ###########################

@app.route('/deconnexion')
def logout():
    # Remove session data, this will log the user out
   session.pop('loggedin', None)
   session.pop('id', None)
   session.pop('nom_utilisateur', None)
   # Redirect to login page
   return redirect(url_for('login'))

   return render_template('deconnexion.html')

if __name__ == '__main__': #si le fichier est executer alors execute le bloc
    app.run(debug=True) #debug=True relance le serveur à chaque modification




#######################################################################################""

# @app.route('/')
# def accueil():

#      return render_template('index.html')



